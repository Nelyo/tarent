import React from 'react';
import {Course} from "./App";
import {Card} from "react-bootstrap";

export function CourseCard(props: Course) {
    return (
        <Card style={{width: '18rem'}}>
            <Card.Body>
                <Card.Title>{props.name}</Card.Title>
                <Card.Text>
                    {props.description}
                </Card.Text>
                <Card.Subtitle>{props.priceInEuro}€</Card.Subtitle>
                <Card.Subtitle>by {props.lecturerName}</Card.Subtitle>
            </Card.Body>
        </Card>
    );
}

