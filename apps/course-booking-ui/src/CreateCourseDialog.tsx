import Modal from "react-bootstrap/esm/Modal";
import {Button, Form} from "react-bootstrap";
import {Course} from "./App";
import {useState} from "react";

export interface Props {
    course: Course,
    closeAction: () => void
    saveAction: (course: Course) => void
}

export function CreateCourseDialog(props: Props) {
    const [state, setState] = useState<Course>(props.course)

    let course = props.course

    return <Modal show={true} onHide={() => props.closeAction()}>
        <Modal.Header closeButton>
            <Modal.Title>Create new course</Modal.Title>
        </Modal.Header>

        <Modal.Body>
            <Form>
                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        onChange={(v) => setState({...state, name: v.target.value})}
                        defaultValue={course.name} placeholder="Enter name"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        onChange={(v) => setState({...state, description: v.target.value})}
                        defaultValue={course.description} placeholder="Enter description"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formLecturerName">
                    <Form.Label>Lecturer</Form.Label>
                    <Form.Control
                        onChange={(v) => setState({...state, lecturerName: v.target.value})}
                        defaultValue={course.lecturerName}
                        placeholder="Enter lecturer"/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formPrice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                        onChange={(v) => setState({...state, priceInEuro: Number.parseFloat(v.target.value)})}
                        defaultValue={course.priceInEuro} type="number"
                        placeholder="Enter price in €"/>
                </Form.Group>
            </Form>
        </Modal.Body>

        <Modal.Footer>
            <Button variant="primary" type={"submit"}
                    onClick={() => props.saveAction({
                        id: state.id,
                        name: state.name,
                        description: state.description,
                        lecturerName: state.lecturerName,
                        priceInEuro: state.priceInEuro
                    } as Course)}>Create</Button>
        </Modal.Footer>
    </Modal>
}
