import React, {useEffect, useState} from 'react';
import './App.css';
import axios from "axios";
import {Alert, Button, Spinner} from "react-bootstrap";
import {CourseCard} from "./Course";
import {v4 as uuidv4} from 'uuid';
import {CreateCourseDialog} from "./CreateCourseDialog";

export interface Course {
    id: string
    name: string
    description: string
    lecturerName: string
    priceInEuro: number
}

const axiosInstance = axios.create({
    baseURL: "http://localhost:3000/api",

    headers: {
        "Content-type": "application/json"
    }
});

function createNewCourse(setDialogCourse: any) {
    setDialogCourse({
        id: uuidv4(),
        name: "",
        description: "",
        lecturerName: "",
        priceInEuro: 0.00
    } as Course);
}

function mapCourse(course: any): Course {
    return {
        id: course.id,
        name: course.name,
        description: course.description,
        lecturerName: course.lecturer_name,
        priceInEuro: course.price_in_euro
    }
}

async function getAllCourses(): Promise<Array<Course>> {
    const {data} = await axiosInstance.get("/courses");
    return data.map(mapCourse)
}

async function createCourse(course: Course): Promise<Course> {
    const requestBody = {
        "id": course.id,
        "name": course.name,
        "description": course.description,
        "lecturer_name": course.lecturerName,
        "price_in_euro": course.priceInEuro,
    }

    const {data} = await axiosInstance.put("/courses", requestBody)

    return mapCourse(data)
}

function Error(props: any) {
    return <div id="errorDiv">
        <Alert variant={'danger'}>
            {props.message}
        </Alert>
    </div>
}

function Loading() {
    return <div id="loadingDiv">
        <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
        </Spinner>
    </div>
}

export function App() {
    const [courses, setCourses] = useState<Array<Course> | undefined>(undefined)
    const [dialogCourse, setDialogCourse] = useState<Course | undefined>(undefined)
    const [loading, setLoading] = useState<boolean>(false)
    const [error, setError] = useState<string | undefined>(undefined)

    useEffect(() => {
        setLoading(true)
        getAllCourses().then(courses => {
            setCourses(courses)
            setError(undefined)
        }).catch(() => {
            setError("Error on loading courses")
        }).finally(() => setLoading(false))
    }, [])

    return (
        <div>
            {loading && <Loading/>
            }
            {courses && <div id="stateDiv">
                {
                    dialogCourse &&
                    <div id="createDialogDiv">
                        <CreateCourseDialog course={dialogCourse}
                                            closeAction={() => setDialogCourse(undefined)}
                                            saveAction={(course) => {
                                                setDialogCourse(undefined)
                                                createCourse(course)
                                                    .then(course => {
                                                        const newCourses = Object.assign([], courses)
                                                        newCourses.push(course)
                                                        setCourses(newCourses)
                                                    })
                                                    .catch(() => {
                                                        setError("Save course failed")
                                                    })
                                            }}
                        />
                    </div>
                }
                <div id="controlHeaderDiv" style={{margin: "10px"}}>
                    <div id="createCourseDiv">
                        <Button variant="primary" onClick={() => createNewCourse(setDialogCourse)}>Create
                            Course</Button>
                    </div>
                </div>
                <div id="courseCards" style={{margin: "10px"}}>
                    {
                        courses.map((course, index) =>
                            <CourseCard id={course.id} name={course.name}
                                        description={course.description}
                                        lecturerName={course.lecturerName}
                                        priceInEuro={course.priceInEuro}
                                        key={"optionCard" + index}/>)
                    }
                </div>
            </div>
            }
            {error && <Error message={error}/>
            }
        </div>
    );
}

export default App;
