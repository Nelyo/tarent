package de.tarent.coursebookingapi.ui;

import de.tarent.coursebookingapi.domain.Course;
import de.tarent.coursebookingapi.domain.CourseRepository;
import de.tarent.coursebookingapi.domain.CourseService;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CourseIntegrationTest {
    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseRepository courseRepository;

    @LocalServerPort
    private int serverPort;

    private final String ID = "6dcad668-a25d-44a7-86cf-af921ea6c825";
    private final String DESCRIPTION = "Some description";
    private final String NAME = "Course for integration test";
    private final String LECTURER_NAME = "Mr. Integrater";
    private final float PRICE_IN_EURO = 99.99f;

    @Test
    public void create_course_should_make_instance_available() {
        verifyCreateCourse();
        verifyGetCourse();
    }

    private void verifyGetCourse() {
        final var response = given().contentType(ContentType.JSON)
                .when().get(getUrl(format("/api/courses/%s", ID)))
                .then().extract().response();

        assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
        final var body = response.getBody().as(CourseRepresentation.class);
        validate(body);
    }

    private void verifyCreateCourse() {
        final var courseRepresentation = new CourseRepresentation(ID, NAME, LECTURER_NAME, DESCRIPTION, PRICE_IN_EURO);
        final var response = given().body(courseRepresentation).contentType(ContentType.JSON)
                .when().put(getUrl("/api/courses"))
                .then().extract().response();

        assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
        final var body = response.getBody().as(CourseRepresentation.class);
        validate(body);

        final var courseOptionalFromService = courseService.get(body.getId());
        validate(courseOptionalFromService.orElseThrow());

        final var courseFromRepository = courseRepository.findById(body.getId());
        validate(courseFromRepository.orElseThrow());
    }

    private void validate(final CourseRepresentation courseRepresentation) {
        assertThat(courseRepresentation.getId()).isEqualTo(ID);
        assertThat(courseRepresentation.getName()).isEqualTo(NAME);
        assertThat(courseRepresentation.getLecturerName()).isEqualTo(LECTURER_NAME);
        assertThat(courseRepresentation.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(courseRepresentation.getPriceInEuro()).isEqualTo(PRICE_IN_EURO);
    }

    private void validate(final Course course) {
        assertThat(course.getId()).isEqualTo(ID);
        assertThat(course.getName()).isEqualTo(NAME);
        assertThat(course.getLecturerName()).isEqualTo(LECTURER_NAME);
        assertThat(course.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(course.getPriceInEuro()).isEqualTo(PRICE_IN_EURO);
    }

    private String getUrl(final String path) {
        return format("http://localhost:%s%s", serverPort, path);
    }
}
