package de.tarent.coursebookingapi.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class EventTest {
    @Test
    void book_on_new_event_should_return_1_attendee() {
        final var course = mock(Course.class);
        final var dateTime = getLocalDateTime();
        final var event = new Event(UUID.randomUUID().toString(), course, dateTime);

        final var success = event.book();

        assertThat(success).isTrue();
        assertThat(event.getCurrentAttendees()).isEqualTo(1);
    }

    @Test
    void book_on_event_with_1_attendee_should_return_2() {
        final var course = mock(Course.class);
        final var dateTime = getLocalDateTime();
        final var event = new Event(UUID.randomUUID().toString(), course, dateTime, 10, 1);
        final var success = event.book();

        assertThat(success).isTrue();
        assertThat(event.getCurrentAttendees()).isEqualTo(2);
    }

    @Test
    void book_on_full_event_should_not_increment() {
        final var course = mock(Course.class);
        final var dateTime = getLocalDateTime();
        final var event = new Event(UUID.randomUUID().toString(), course, dateTime, 10, 10);
        final var success = event.book();

        assertThat(success).isFalse();
        assertThat(event.getCurrentAttendees()).isEqualTo(10);
    }

    private LocalDateTime getLocalDateTime() {
        return LocalDateTime.of(2020, 1, 1, 1, 1, 1);
    }
}