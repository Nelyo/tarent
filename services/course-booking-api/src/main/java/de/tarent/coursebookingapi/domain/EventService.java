package de.tarent.coursebookingapi.domain;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(final EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Optional<Event> get(final String id) {
        return this.eventRepository.findById(id);
    }

    public List<Event> getAll(final String courseId) {
        return this.eventRepository.findAll(courseId);
    }

    public List<Event> getAll(final LocalDateTime from, final LocalDateTime to) {
        return this.eventRepository.findAll(from, to);
    }

    public Event createOrModify(final Event event) {
        return this.eventRepository.save(event);
    }
}
