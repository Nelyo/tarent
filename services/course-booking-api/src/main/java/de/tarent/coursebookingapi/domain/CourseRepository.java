package de.tarent.coursebookingapi.domain;

import java.util.List;
import java.util.Optional;

public interface CourseRepository {
    Course save(final Course course);

    List<Course> findAll();

    Optional<Course> findById(final String id);
}
