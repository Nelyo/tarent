package de.tarent.coursebookingapi.ui;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.tarent.coursebookingapi.domain.Course;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class CourseRepresentation {
    @JsonProperty("id") private String id;
    @JsonProperty("name") private String name;
    @JsonProperty("lecturer_name") private String lecturerName;
    @JsonProperty("description") private String description;
    @JsonProperty("price_in_euro") private Float priceInEuro;

    public CourseRepresentation(final Course course) {
        this.id = course.getId();
        this.name = course.getName();
        this.lecturerName = course.getLecturerName();
        this.description = course.getDescription();
        this.priceInEuro = course.getPriceInEuro();
    }
}
