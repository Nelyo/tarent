package de.tarent.coursebookingapi.ui;

import de.tarent.coursebookingapi.domain.CourseService;
import de.tarent.coursebookingapi.domain.EventService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EventRestController {
    private final EventService eventService;
    private final CourseService courseService;

    EventRestController(final EventService eventService, final CourseService courseService) {
        this.eventService = eventService;
        this.courseService = courseService;
    }

    @GetMapping("/events")
    ResponseEntity<List<EventRepresentation>> getEvents(@RequestParam(value = "course_id", required = false) final String courseId) {
        return ResponseEntity.ok(eventService.getAll(courseId).stream()
                                         .map(EventRepresentation::new)
                                         .collect(Collectors.toList()));
    }

    @GetMapping("/events/{id}")
    ResponseEntity<EventRepresentation> getEvent(@PathVariable final String id) {
        return eventService.get(id)
                .map(event -> ResponseEntity.ok(new EventRepresentation(event)))
                .orElse(ResponseEntity.notFound().build());
    }


    @PutMapping("/events")
    ResponseEntity<EventRepresentation> createOrReplace(@RequestBody final EventRepresentation eventRepresentation) {
        final var course = courseService.get(eventRepresentation.getCourseId()).orElse(null);

        if (course == null) {
            return ResponseEntity.unprocessableEntity().build();
        }

        final var event = eventRepresentation.toDomainObjectWith(course);

        return ResponseEntity.ok(new EventRepresentation(eventService.createOrModify(event)));
    }

    @PostMapping("/events/{event_id}/book")
    ResponseEntity<Void> book(@PathVariable("event_id") final String eventId) {
        final var event = eventService.get(eventId);

        if (event.isPresent()) {
            final var success = event.get().book();

            if (success) {
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        }

        return ResponseEntity.notFound().build();
    }
}
