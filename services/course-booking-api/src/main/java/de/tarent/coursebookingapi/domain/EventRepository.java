package de.tarent.coursebookingapi.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventRepository {
    Event save(final Event event);

    List<Event> findAll(final String courseId);

    Optional<Event> findById(final String id);

    List<Event> findAll(final LocalDateTime from, final LocalDateTime to);
}
