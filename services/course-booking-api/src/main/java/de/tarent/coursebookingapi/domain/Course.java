package de.tarent.coursebookingapi.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(of = "id")
public class Course {
    private final String id;
    private final String name;
    private final String lecturerName;
    private final Float priceInEuro;
    private final String description;
}