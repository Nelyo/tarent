package de.tarent.coursebookingapi.ui;

import de.tarent.coursebookingapi.domain.Course;
import de.tarent.coursebookingapi.domain.CourseService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CourseRestController {
    private final CourseService courseService;

    CourseRestController(final CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/courses/{id}")
    ResponseEntity<CourseRepresentation> getCourse(@PathVariable final String id) {
        return courseService.get(id)
                .map(course -> ResponseEntity.ok(new CourseRepresentation(course)))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/courses")
    ResponseEntity<List<CourseRepresentation>> getCourses(
            @RequestParam(value = "from_datetime", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime from,
            @RequestParam(value = "to_datetime", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime to) {

        if (areBothNullOrNone(from, to)) {
            return ResponseEntity.ok(
                    courseService.getAll(from, to).stream().map(CourseRepresentation::new)
                            .collect(Collectors.toList()));
        }

        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/courses")
    ResponseEntity<CourseRepresentation> createOrReplace(@RequestBody final CourseRepresentation courseRepresentation) {
        final var course = toDomainObject(courseRepresentation);

        return ResponseEntity.ok(new CourseRepresentation(courseService.createOrModify(course)));
    }

    private Course toDomainObject(final CourseRepresentation c) {
        return new Course(c.getId(), c.getName(), c.getLecturerName(), c.getPriceInEuro(), c.getDescription());
    }

    private boolean areBothNullOrNone(final LocalDateTime from, final LocalDateTime to) {
        return from == null && to == null || from != null && to != null;
    }
}
