package de.tarent.coursebookingapi.domain;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class InMemoryCourseRepository implements CourseRepository {
    private final Map<String, Course> courses;

    InMemoryCourseRepository() {
        courses = new LinkedHashMap<>();
    }

    @Override
    public Course save(final Course course) {
        courses.put(course.getId(), course);

        return course;
    }

    @Override
    public List<Course> findAll() {
        return new ArrayList<>(courses.values());
    }

    @Override
    public Optional<Course> findById(final String id) {
        return Optional.ofNullable(courses.get(id));
    }
}
