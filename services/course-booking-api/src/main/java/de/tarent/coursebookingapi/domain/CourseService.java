package de.tarent.coursebookingapi.domain;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CourseService {
    private final CourseRepository courseRepository;
    private final EventService eventService;

    CourseService(final CourseRepository courseRepository, final EventService eventService) {
        this.courseRepository = courseRepository;
        this.eventService = eventService;
    }

    public Optional<Course> get(final String id) {
        return this.courseRepository.findById(id);
    }

    public List<Course> getAll(final LocalDateTime from, final LocalDateTime to) {
        if (from != null && to != null) {
            return eventService.getAll(from, to).stream()
                    .map(Event::getCourse)
                    .distinct()
                    .collect(Collectors.toList());
        }

        return this.courseRepository.findAll();
    }

    public Course createOrModify(final Course course) {
        return this.courseRepository.save(course);
    }
}