package de.tarent.coursebookingapi.ui;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.tarent.coursebookingapi.domain.Course;
import de.tarent.coursebookingapi.domain.Event;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class EventRepresentation {
    @JsonProperty("id") private String id;
    @JsonProperty("course_id") private String courseId;
    @JsonProperty("started_at") private LocalDateTime startedAt;
    @JsonProperty(value = "max_attendees", access = JsonProperty.Access.READ_ONLY) private Integer maxAttendees;
    @JsonProperty(value = "current_attendees", access = JsonProperty.Access.READ_ONLY) private Integer currentAttendees;

    public EventRepresentation(final Event event) {
        this.id = event.getId();
        this.courseId = event.getCourse().getId();
        this.startedAt = event.getStartedAt();
        this.maxAttendees = event.getMaxAttendees();
        this.currentAttendees = event.getCurrentAttendees();
    }

    public Event toDomainObjectWith(final Course course) {
        return new Event(this.getId(), course, this.getStartedAt());
    }
}
