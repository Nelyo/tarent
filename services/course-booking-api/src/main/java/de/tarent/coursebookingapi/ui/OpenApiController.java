package de.tarent.coursebookingapi.ui;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.io.InputStream;

@RestController
public class OpenApiController {
    private final static String V1_MEDIA_TYPE = "application/vnd.course-booking.v1+yaml";

    @GetMapping(path = {"/"})
    public RedirectView redirectToSwaggerUI() {
        return new RedirectView("swagger-ui.html");
    }

    @GetMapping(path = "/openapi")
    public RedirectView redirectToYamlFile() {
        return new RedirectView("openapi.yaml");
    }

    @GetMapping(path = "/openapi.yaml")
    public ResponseEntity<String> getOpenApi() {
        return ResponseEntity.ok()
                .header("Content-Type", V1_MEDIA_TYPE)
                .body(getOpenapi());
    }

    private String getOpenapi() {
        InputStream inputStream = null;
        ByteArrayResource resource = null;
        try {
            inputStream = (InputStream) getClass().getResource("/openapi.yaml").getContent();
            resource = new ByteArrayResource(IOUtils.toByteArray(inputStream));
        } catch (final IOException e) {
            throw new IllegalStateException("Failed to load Open API");
        }

        return new String(resource.getByteArray());
    }
}