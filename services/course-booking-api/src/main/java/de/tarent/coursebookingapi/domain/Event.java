package de.tarent.coursebookingapi.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class Event {
    public static final int MAX_ATTENDEES = 10;

    private final String id;
    private final Course course;
    private final LocalDateTime startedAt;
    private final Integer maxAttendees;
    private Integer currentAttendees;

    public Event(final String id, final Course course, final LocalDateTime startedAt) {
        this.id = id;
        this.course = course;
        this.startedAt = startedAt;
        this.maxAttendees = MAX_ATTENDEES;
        this.currentAttendees = 0;
    }

    synchronized public boolean book() {
        if (currentAttendees < maxAttendees) {
            currentAttendees += 1;
            return true;
        }

        return false;
    }
}
