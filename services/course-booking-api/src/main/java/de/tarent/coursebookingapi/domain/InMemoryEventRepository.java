package de.tarent.coursebookingapi.domain;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class InMemoryEventRepository implements EventRepository {
    private final Map<String, Event> events;

    InMemoryEventRepository() {
        events = new LinkedHashMap<>();
    }

    @Override
    public Event save(final Event event) {
        events.put(event.getId(), event);
        return event;
    }

    @Override
    public List<Event> findAll(final String courseId) {
        if (StringUtils.isBlank(courseId)) {
            return new ArrayList<>(events.values());
        }

        return events.values().stream()
                .filter(e -> e.getCourse().getId().equalsIgnoreCase(courseId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Event> findAll(final LocalDateTime from, final LocalDateTime to) {
        return events.values().stream()
                .filter(e -> e.getStartedAt().isAfter(from) && e.getStartedAt().isBefore(to))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Event> findById(final String id) {
        return Optional.ofNullable(events.get(id));
    }
}
